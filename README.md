# Process report Starter

> Dependency management is a critical aspects of any complex project. And doing this manually is less than ideal; the more time you spent on it the less time you have on the other important aspects of the project.
> Process report Starter were built to address exactly this problem. You get a one-stop-shop for all the functionality that you need to audit your process flow.

![Event Reporting Workflow](Resources/reporting-service-building-block.png)

![Event Reporting Porcess View](Resources/reporting-01.png)

![Event Reporting Instance View](Resources/reporting-02.png)

![Event Reporting Instance Details View](Resources/reporting-03.png)

![Event Reporting Porcess Diagram View](Resources/reporting-04.png)

![Event Reporting Porcess Report View](Resources/reporting-05.png)

## Requirements

* Maven dependency
  ```
    <dependency>
        <groupId>de.microtema</groupId>
        <artifactId>process-reporting-starter</artifactId>
    </dependency>
  ```
* @BpmnElement
  ```
    @BpmnElement(id = "Event_1anjljr", startEvent = true, keyExpression = "{{PO_NUMBER}}")
    public PurchaseOrder execute(PurchaseOrderEvent event) {

        return ...;
    }

    ----

    @BpmnElement(id = "Activity_194xhhj")
    public ProcurementOrder execute(PurchaseOrder data) {

        return ...;
    }

    ---

    @BpmnElement(id = "Event_0byexdn", endEvent = true)
    public ProcurementOrder execute(ProcurementOrder procurementOrder, boolean purchaseOrderExists) {

        return ...;
    }
  ```
* application.yaml
  ```
  reporting:
    process-id: Process_0e2hw2v
    server: http://localhost:9090/reporting-service/rest
    process-version: 1.0
  ```
* BPMN process-flow.bpmn
  * ![Event Reporting Workflow](Resources/process-flow-diagram.png)

## Testing

### Unit Test

```mvn test```

## API

### REST Report API

```
@POST ${reporting.server}/api/report

@BODY {
    ...@ReportEvent
}
```

### REST Process API

```
@POST ${reporting.server}/api/process

@BODY {
    ...@ProcessEvent
}
```

### REST Heart Beat API

```
@POST ${reporting.server}/api/definition/heart-beat

@BODY {
    ...@HeartBeatEvent
}
```

### REST Process Definition API

```
@POST ${reporting.server}/api/definition

@BODY {
    ...@RegisterEvent
}
```

### Get Request object anywhere in Spring WebFlux or MVC

In a normal Spring Web project, it is very easy to get the Request object and many libraries provide static methods to
get it.

The code to get it is as follows.

```
ServletRequestAttributes requestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
// get the request
HttpServletRequest request = requestAttributes.getRequest();
```

But in the responsive WebFlux world, there is no similar Holder class provided, and WebFlux is not thread-aware; any
thread can handle any request at any time, and if it feels more efficient to switch the current thread, it will do so.
But in a Servlet Based application, it will schedule a thread for a request to handle the entire process.

This huge difference means that you can’t simply save and fetch Request through ThreadLocal anymore.

A look at the official documentation for reactor shows the following passage.

Since version 3.1.0 , Reactor comes with an advanced feature that is somewhat comparable to ThreadLocal but can be
applied to a Flux or a Mono instead of a Thread . This feature is called Context .

#### Implementation

First, get the Request object in the WebFilter and save it, the code is as follows.

```
@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
public class ReactiveRequestContextFilter implements WebFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        return chain.filter(exchange)
                .subscriberContext(ctx -> ctx.put(ReactiveRequestContextHolder.CONTEXT_KEY, request));
    }
}
```

#### Tool Classes - Holder

Implement a tool class to provide static methods that can be used in any scenario after Filter.

```
public class ReactiveRequestContextHolder {
   
    public static final Class<ServerHttpRequest> CONTEXT_KEY = ServerHttpRequest.class;

    public static Mono<ServerHttpRequest> getRequest() {
        return Mono.subscriberContext()
                .map(ctx -> ctx.get(CONTEXT_KEY));
    }
}
```

#### Using in Controller/Service

```
@RestController
public class GetRequestController {

    @RequestMapping("/request")
    public Mono<String> getRequest() {
        return ReactiveRequestContextHolder.getRequest()
                .map(request -> request.getHeaders().getFirst("user"));
    }
}
```

### Reporting Properties

|Name|Type|Required|Default Value|Description|
| --- | --- | --- | --- | --- |
| processId | String | true |  | Unique Process Id |
| processName | String | true |  | Process Name |
| processVersion | String | true | 1.0 | Process Version |
| server | String | true |  | Process Reporting Service |
| heartBeatCron | String | false | 0 */2 * ? * * | report heart beat for current service |
| maxEventContentLimit | int | false | 2000 | Max event content size |

### BaseReportEvent

|Name|Type|Required|Default Value|Description|
| --- | --- | --- | --- | --- |
| transactionId | String | true |  | Unique Transaction Id generated by system|
| processId | String | true |  | Unique Process Id |
| processVersion | String | true | 1.0 | Process Version |
| executionId | String | true |  | Process Unique execution Id generated by system|
| errorMessage | String | false |  | Error Message during execution |
| status | ReportStatus | false |  | report status STARTED, COMPLETED, ... |
| eventTime | LocalDateTime | true | now | Event time generated by system |
| retryCount | int | false | 0 | Retry Count |
| partCount | int | false | 0 | Part Count im Event maxEventContentLimit exceeded |

### ProcessEvent extends BaseReportEvent

|Name|Type|Required|Default Value|Description|
| --- | --- | --- | --- | --- |
| eventTriggeredTime | LocalDateTime | true |  | Event Triggered time from source system |
| startBy | String | true |  | who started the process? |
| boundedContext | String | true |  | Bounded Context that process belong to |
| eventType | String | true |  | Event Type CREATED, UPDATED, DELETED, ... |
| eventSource | String | true |  | Event Source  who throw this event? (System-A) |
| runtimeId | String | true |  | Runtime Id generated by system |
| referenceId | String | true |  | Reference Id |

### ReportEvent extends BaseReportEvent

|Name|Type|Required|Default Value|Description|
| --- | --- | --- | --- | --- |
| elementId | String | true |  | BPMN Element unique Id |
| multipleInstanceIndex | String | true |  | Task may be executed multiple times |
| payload | String | true |  | Event Payload like Json or primitive Objects |

### HeartBeatEvent

|Name|Type|Required|Default Value|Description|
| --- | --- | --- | --- | --- |
| processId | String | true |  | Unique Process Id |
| processVersion | String | true | 1.0 | Process Version |
| eventTime | LocalDateTime | true | 0 | Event time generated by system |

### RegisterEvent

|Name|Type|Required|Default Value|Description|
| --- | --- | --- | --- | --- |
| processDiagram | String | true |  | Process XML Diagram |
| processId | String | true |  | Unique Process Id |
| processVersion | String | true | 1.0 | Process Version |
| boundedContext | String | true |  | Bounded Context that process belong to |
| fileName | String | true |  | Process XML Diagram file name |

### ReportStatus

|Type|Description|
| --- | --- |
| QUEUED | Process has been queued |
| STARTED | Process/Task has been started |
| RESTARTED | Process/Task has been restarted |
| COMPLETED | Process/Task has been completed |
| WARNING | Task has ended with warning but Process keep running |
| ERROR | Task has ended with error and Process has been terminated |

## Technology Stack

* Java 13
    * Streams
    * Lambdas
* Third Party Libraries
    * Commons-Lang3 (Apache License)
    * model-converter (MIT License)
    * Junit (EPL 1.0 License)
* Code-Analyses
    * Sonar
    * Jacoco

## License

MIT (unless noted otherwise)
