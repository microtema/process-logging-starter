package de.microtema.process.logging.converter;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.util.FieldInjectionUtil;
import jakarta.inject.Inject;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.message.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LogEventToLogReportEventConverterTest {

    @Inject
    LogEventToLogReportEventConverter sut;

    @Mock
    LogEvent model;

    @Mock
    Message message;

    Level level = Level.INFO;

    @Model
    String messageText;

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void update() {

        when(model.getLevel()).thenReturn(level);
        when(model.getMessage()).thenReturn(message);
        when(message.getFormattedMessage()).thenReturn(messageText);
        when(model.getThrown()).thenReturn(new NullPointerException("NPE"));

        var answer = sut.convert(model);

        assertNotNull(answer);
        assertEquals(level.name(), answer.getLevel());
        assertEquals(messageText, answer.getMessage());
        assertNotNull(answer.getStackTrace());
    }
}
