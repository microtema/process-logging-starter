package de.microtema.process.logging.service;


import de.microtema.process.logging.config.ReportingProperties;
import de.microtema.process.logging.converter.LogEventToLogReportEventConverter;
import de.microtema.process.logging.model.LogReportEvent;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.core.LogEvent;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Component
public class ReportLogService {

    private final String hostName;
    private final String applicationName;
    private final RestTemplate restTemplate;
    private final ReportingProperties reportingProperties;
    private final LogEventToLogReportEventConverter logEventToLogReportEventConverter;

    public ReportLogService(Environment environment, LogEventToLogReportEventConverter logEventToLogReportEventConverter, RestTemplate restTemplate, ReportingProperties reportingProperties) {
        hostName = Optional.ofNullable(System.getenv("HOSTNAME")).orElseGet(() -> System.getProperty("PID"));
        applicationName = Stream.of(reportingProperties.getProcessName(), reportingProperties.getProcessId()).filter(Objects::nonNull).findFirst().orElseGet(() -> environment.getProperty("spring.application.name"));
        this.logEventToLogReportEventConverter = logEventToLogReportEventConverter;
        this.reportingProperties = reportingProperties;
        this.restTemplate = restTemplate;
    }

    @Async
    public void fireLogEvent(LogEvent event) {

        var logEvent = logEventToLogReportEventConverter.convert(event);

        logEvent.setRuntimeId(hostName);
        logEvent.setRuntimeName(applicationName);

        var endpointUrl = reportingProperties.getServer() + "/api/log";

        var request = createRequest(logEvent);

        var response = restTemplate.postForEntity(endpointUrl, request, String.class);

        Validate.isTrue(response.getStatusCode() == HttpStatus.OK, "Unsupported status!");
    }

    private HttpEntity<LogReportEvent> createRequest(LogReportEvent logEvent) {

        var headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return new HttpEntity<>(logEvent, headers);
    }
}
