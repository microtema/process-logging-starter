package de.microtema.process.logging.converter;

import de.microtema.model.converter.Converter;
import de.microtema.process.logging.model.LogReportEvent;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.message.Message;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

@Component
public class LogEventToLogReportEventConverter implements Converter<LogReportEvent, LogEvent> {

    @Override
    public void update(LogReportEvent dest, LogEvent orig) {

        dest.setLevel(orig.getLevel().name());
        dest.setMessage(getMessage(orig.getMessage()));
        dest.setStackTrace(getStackTrace(orig.getThrown()));

        dest.setTimestamp(millsToLocalDateTime(orig.getTimeMillis()));
    }

    private LocalDateTime millsToLocalDateTime(long millis) {

        var instant = Instant.ofEpochMilli(millis);

        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    private String getMessage(Message message) {

        return Optional.ofNullable(message.getFormattedMessage()).orElseGet(() -> ExceptionUtils.getRootCauseMessage(message.getThrowable()));
    }

    private String getStackTrace(Throwable throwable) {

        return Optional.ofNullable(throwable).map(ExceptionUtils::getStackTrace).orElse(null);
    }
}
