package de.microtema.process.logging.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LogReportEvent {

    private String runtimeName;

    private String runtimeId;

    private String message;

    private String stackTrace;

    private String level;

    private LocalDateTime timestamp;
}
