package de.microtema.process.logging.appender;

import de.microtema.process.logging.config.ReportLogConfig;
import de.microtema.process.logging.service.ReportLogService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import java.util.Objects;

@Log4j2
@Plugin(name = "ReportLog", category = Core.CATEGORY_NAME)
public final class ReportLogAppender extends AbstractAppender {

    private static ReportLogAppender instance;

    private ReportLogService reportLogService;

    protected ReportLogAppender(String name) {
        super(name, null, null, true, Property.EMPTY_ARRAY);
    }

    @PluginFactory
    public static ReportLogAppender createAppender(@PluginAttribute("name") String name) {
        instance = new ReportLogAppender(name);
        return instance;
    }

    @Override
    public void append(LogEvent event) {

        if (Objects.isNull(instance.reportLogService)) {

            try {
                instance.reportLogService = ReportLogConfig.getBean(ReportLogService.class);
            } catch (Exception e) {
                return;
            }
        }

        if (!log.isEnabled(event.getLevel())) {
            return;
        }

        instance.reportLogService.fireLogEvent(event);
    }
}
