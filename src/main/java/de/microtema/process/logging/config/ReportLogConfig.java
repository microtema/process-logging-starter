
package de.microtema.process.logging.config;


import de.microtema.process.logging.converter.LogEventToLogReportEventConverter;
import de.microtema.process.logging.service.ReportLogService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {
        ReportLogService.class,
        LogEventToLogReportEventConverter.class,
        ReportingProperties.class})
public class ReportLogConfig implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    @Override
    public void setApplicationContext(ApplicationContext pApplicationContext) throws BeansException {
        applicationContext = pApplicationContext;
    }

}
