package de.microtema.process.logging.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "reporting")
public class ReportingProperties {

    private String processId;

    private String processName;

    private String processVersion;

    private String server;
}
